﻿using System.Collections.Generic;
using System.Linq;
using NuclearPowerPlantElements.Model;
using NuclearPowerPlantElements.Model.DataSources;
using NuclearPowerPlantElements.ViewModel.Elements;

namespace NuclearPowerPlantElements.ViewModel.Infrastructure.Factory
{
    public static class ReactorViewModelFactory
    {
        public static ReactorViewModel CreateReactor()
        {
            var reactorDataSource = new ReactorDataSource();

            var viewModel = new ReactorViewModel();

            viewModel.AllCells =
                reactorDataSource.CreateReactorCells()
                    .OrderByDescending(c => c.Y)
                    .GroupBy(c => c.Y, (key, cell) => OrderList(cell.ToList()))
                    .ToList();

            return viewModel;
        }

        private static List<Cell> OrderList(List<Cell> basicCells)
        {
            basicCells = basicCells.OrderBy(c => c.X).ToList();

            return basicCells;
        } 
    }
}
