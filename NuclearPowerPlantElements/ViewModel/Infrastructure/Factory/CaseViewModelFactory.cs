﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NuclearPowerPlantElements.Model;
using NuclearPowerPlantElements.Model.DataSources;
using NuclearPowerPlantElements.ViewModel.Elements;

namespace NuclearPowerPlantElements.ViewModel.Infrastructure.Factory
{
    public static class CaseViewModelFactory
    {
        public static CaseViewModel CreateCase()
        {
            var dataSource = new CaseDataSource();
            var viewModel = new CaseViewModel() { AllCells = GetMatrix(dataSource.CreateCaseCells())};

            return viewModel;
        }

        private static List<List<Cell>> GetMatrix(List<Cell> cells)
        {
            var result = new List<List<Cell>>();

            var allCells = cells.OrderBy(c => c.X);

            var skipCount = 0;
            for (int i = 0; i < 5; i++)
            {
                result.Add(new List<Cell>());

                if (i % 2 == 0)
                {
                    result[i].AddRange(allCells.Skip(skipCount).Take(5));
                    skipCount += 5;
                }
                else
                {
                    result[i].AddRange(allCells.Skip(skipCount).Take(6));
                    skipCount += 6;
                }
            }

            result.Reverse();

            return result;
        }
    }
}
