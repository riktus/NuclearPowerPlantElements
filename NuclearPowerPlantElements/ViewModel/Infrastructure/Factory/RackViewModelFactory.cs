﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NuclearPowerPlantElements.Model;
using NuclearPowerPlantElements.Model.DataSources;
using NuclearPowerPlantElements.ViewModel.Elements;

namespace NuclearPowerPlantElements.ViewModel.Infrastructure.Factory
{
    public static class RackViewModelFactory
    {
        public static RackViewModel CreateRack()
        {
            var dataSource = new RackDataSource();
            var viewModel = new RackViewModel() { AllCells = GetMatrix(dataSource.CreateRackCells()) };

            return viewModel;
        }

        private static List<List<Cell>> GetMatrix(List<Cell> cells)
        {
            var result = new List<List<Cell>>();

            var allCells = cells.OrderBy(c => c.X);

            var skipCount = 0;
            for (int i = 0; i < 7; i++)
            {
                result.Add(new List<Cell>());

                if (i % 2 == 0)
                {
                    result[i].AddRange(allCells.Skip(skipCount).Take(10));
                    skipCount += 10;
                }
                else
                {
                    result[i].AddRange(allCells.Skip(skipCount).Take(9));
                    skipCount += 9;
                }
            }

            result.Reverse();

            return result;
        }
    }
}
