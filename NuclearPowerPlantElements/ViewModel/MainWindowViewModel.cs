﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NuclearPowerPlantElements.ViewModel.Elements;
using NuclearPowerPlantElements.ViewModel.Infrastructure.Factory;

namespace NuclearPowerPlantElements.ViewModel
{
    public class MainWindowViewModel
    {
        public ReactorViewModel ReactorViewModel { get; private set; }
        public RackViewModel RackViewModel { get; private set; }
        public CaseViewModel CaseViewModel { get; private set; }

        public MainWindowViewModel()
        {
            InitViewModel();
        }

        private void InitViewModel()
        {
            ReactorViewModel = ReactorViewModelFactory.CreateReactor();
            RackViewModel = RackViewModelFactory.CreateRack();
            CaseViewModel = CaseViewModelFactory.CreateCase();
        }
    }
}
