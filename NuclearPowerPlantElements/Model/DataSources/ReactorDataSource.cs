﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuclearPowerPlantElements.Model.DataSources
{
    public class ReactorDataSource
    {
        public List<Cell> CreateReactorCells()
        {
            var allCells = new List<Cell>();

            allCells.AddRange(CreateCells(16, new[] { 7, 9 }));
            allCells.AddRange(CreateCells(17, new[] { 6, 8, 10 }));
            allCells.AddRange(CreateCells(18, new[] { 5, 7, 9, 11 }));
            allCells.AddRange(CreateCells(19, new[] { 4, 6, 8, 10, 12 }));
            allCells.AddRange(CreateCells(20, new[] { 3, 5, 7, 9, 11, 13 }));
            allCells.AddRange(CreateCells(21, new[] { 2, 4, 6, 8, 10, 12, 14 }));
            allCells.AddRange(CreateCells(22, new[] { 3, 5, 7, 9, 11, 13 }));
            allCells.AddRange(CreateCells(23, new[] { 2, 4, 6, 8, 10, 12, 14 }));
            allCells.AddRange(CreateCells(24, new[] { 1, 3, 5, 7, 9, 11, 13, 15 }));
            allCells.AddRange(CreateCells(25, new[] { 2, 4, 6, 8, 10, 12, 14 }));
            allCells.AddRange(CreateCells(26, new[] { 1, 3, 5, 7, 9, 11, 13, 15 }));
            allCells.AddRange(CreateCells(27, new[] { 2, 4, 6, 8, 10, 12, 14 }));
            allCells.AddRange(CreateCells(28, new[] { 1, 3, 5, 7, 9, 11, 13, 15 }));
            allCells.AddRange(CreateCells(29, new[] { 2, 4, 6, 8, 10, 12, 14 }));
            allCells.AddRange(CreateCells(30, new[] { 1, 3, 5, 7, 9, 11, 13, 15 }));
            allCells.AddRange(CreateCells(31, new[] { 2, 4, 6, 8, 10, 12, 14 }));
            allCells.AddRange(CreateCells(32, new[] { 1, 3, 5, 7, 9, 11, 13, 15 }));
            allCells.AddRange(CreateCells(33, new[] { 2, 4, 6, 8, 10, 12, 14 }));
            allCells.AddRange(CreateCells(34, new[] { 1, 3, 5, 7, 9, 11, 13, 15 }));
            allCells.AddRange(CreateCells(35, new[] { 2, 4, 6, 8, 10, 12, 14 }));
            allCells.AddRange(CreateCells(36, new[] { 3, 5, 7, 9, 11, 13 }));
            allCells.AddRange(CreateCells(37, new[] { 2, 4, 6, 8, 10, 12, 14 }));
            allCells.AddRange(CreateCells(38, new[] { 3, 5, 7, 9, 11, 13 }));
            allCells.AddRange(CreateCells(39, new[] { 4, 6, 8, 10, 12 }));
            allCells.AddRange(CreateCells(40, new[] { 5, 7, 9, 11 }));
            allCells.AddRange(CreateCells(41, new[] { 6, 8, 10 }));
            allCells.AddRange(CreateCells(42, new[] { 7, 9 }));

            return allCells;
        }

        private static IEnumerable<Cell> CreateCells(int y, int[] indexes)
        {
            return indexes.Select(index => new Cell() { X = index, Y = y}).ToList();
        }
    }
}
