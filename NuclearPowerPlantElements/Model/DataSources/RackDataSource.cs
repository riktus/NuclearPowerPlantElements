﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuclearPowerPlantElements.Model.DataSources
{
    public class RackDataSource
    {
        public List<Cell> CreateRackCells()
        {
            var result = new List<Cell>();

            for (int i = 1; i < 68; i++)
            {
                result.Add(new Cell() { X = i, Y = 0 });
            }

            return result;
        } 
    }
}
