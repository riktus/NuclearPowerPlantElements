﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuclearPowerPlantElements.Model
{
    public class Cell
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}
